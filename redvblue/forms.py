from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth import authenticate
import re
from redvblue.models import Team


class LoginForm(forms.Form):
    username = forms.CharField(max_length=100, required=True)
    password = forms.CharField(label='Password', max_length=100, required=True, widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super().clean()
        user = authenticate(
            username=cleaned_data.get('username'),
            password = cleaned_data.get('password'))
        if user is None:
            self._errors['username'] = self.error_class(['You sure?'])
            self._errors['password'] = self.error_class(['Wrong wrong wrong'])
            raise ValidationError('User doesn\'t exist. Or details entered incorrectly')
        return self.cleaned_data


class UserForm(forms.Form):
    display_name = forms.CharField(label='Display Name', max_length=100, required=True)
    username = forms.CharField(label='Username', max_length=100, required=True)
    password = forms.CharField(widget=forms.PasswordInput, label='Password', max_length=100, required=True)
    avatar = forms.FileField(label='Avatar', required=True)

    def clean(self):
        display_name_regex = r'[A-Za-z -]'
        user_regex = r'[A-Za-z0-9@#.]'
        cleaned_data = super().clean()
        display_name = cleaned_data.get('display_name')
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        if len(password) <= 8:
            self._errors['password'] = self.error_class([' Must be longer than 8 characters'])
        if not re.match(display_name_regex, display_name):
            self._errors['display_name'] = self.error_class(['Wrong format, see below'])
        if not re.match(user_regex, username):
            self._errors['username'] = self.error_class(['Wrong format, see below'])
        return self.cleaned_data


class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = (
                'name',
                'color',
                'admin',
                'avatar_img',)
    def clean(self):
        cleaned_data = super().clean()
        team_name = cleaned_data.get('team_name')
        team_color = cleaned_data.get('team_color')
        kregex = r'[A-Za-z0-9]'
        if not team_color:
            self.errors['team_color'] = self.error_class(['Don\'t forget team color'])
        if not re.match(kregex, team_name):
            self.errors['team_name'] = self.error_class(['Name must be Alphanumeric'])
        return self.cleaned_data
