from . import consumers
from django.urls import path
websocket_urlpatterns = [
    path('ws/redvblue/chatroom/<int:pk>/', consumers.ChatConsumer.as_asgi()),
]