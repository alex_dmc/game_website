from django.contrib.auth.models import User
from django.db import models


class Team(models.Model):
    name = models.CharField(max_length=200, unique=True)
    color = models.CharField(max_length=200)
    member_count = models.IntegerField(default=1)
    admin = models.ForeignKey('Player', on_delete=models.CASCADE, related_name='admin')
    avatar_img = models.ImageField(upload_to='team_avi', null=False, blank=False)

    class Meta:
        unique_together = ["name", "color"]

    def collect_self_members(self):
        self.member_count = len(self.player_set.all())
        self.save()

    def __str__(self):
        return self.name


class Player(models.Model):
    STATUS = (
            (0, 'Registered'),
            (1, 'Requested'),
            (2, 'Member'),
            (3, 'Rejected'),
            )
    display_name = models.CharField(max_length=200)
    team = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, blank=True)
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
    status = models.PositiveSmallIntegerField(default=0, choices=STATUS)
    avatar_img = models.ImageField(upload_to='player_avi', null=False, blank=False)

    def save(self, *args, **kwargs):
        super(Player, self).save(*args, **kwargs)
        if self.team:
            self.team.collect_self_members()

    def __str__(self):
        return self.display_name


    @property
    def message(self):
        if self.status == 0:
            team_message = 'You have no Team, time to join or create one'
            msg_short = 'Join Team'
            btn_color = 'btn btn-success'
            text_color = 'green'
        elif self.status == 1:
            team_message = 'Your request to Join a Team is Pending Approval'
            msg_short = 'Pending'
            btn_color = 'btn btn-warning'
            text_color = 'yellow'
        elif self.status == 2:
            team_message = 'Welcome ' + self.__str__() + ', what would you like to do?'
            msg_short = ''
            btn_color = ''
            text_color = 'black'
        elif self.status == 3:
            team_message = 'You Were removed from team, Request Denied/Team Deleted'
            msg_short = 'Join Team'
            btn_color = 'btn btn-success'
            text_color = 'red'
        messages = {'team_message': team_message, 'btn_color': btn_color, 'msg_short': msg_short, 'text_color': text_color}
        return messages


class Messages(models.Model):
    team = models.ForeignKey(Team, related_name='team', on_delete=models.CASCADE)
    author = models.ForeignKey(Player, related_name='messages', on_delete=models.CASCADE)
    context = models.TextField()
    date = models.DateTimeField(default='timezone.now')

    def __str__(self):
        return self.author.user.username

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'
