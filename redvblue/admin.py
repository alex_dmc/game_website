from django.contrib import admin
from .models import Team, Player, Messages


class TeamDisplay(admin.ModelAdmin):
    list_display = ('name', 'admin', 'member_count')
    list_filter = (
        ('admin', admin.RelatedOnlyFieldListFilter),
    )


class PlayerDisplay(admin.ModelAdmin):
    list_display = ('user', 'team',)
    list_filter = (
        ('team', admin.RelatedOnlyFieldListFilter),
    )


class MessageDisplay(admin.ModelAdmin):
    list_display = ('author', 'team', 'date',)


admin.site.register(Player, PlayerDisplay)
admin.site.register(Team, TeamDisplay)
admin.site.register(Messages, MessageDisplay)

