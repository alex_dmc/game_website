import sys
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from redvblue.models import Player, Team, Messages
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from redvblue.forms import LoginForm, TeamForm, UserForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse
from django.conf import settings


def home(request):
    message = request.user.player.message if hasattr(request.user, 'player') else {'welcome_message': 'Hello, Please Login/Register', 'text_color': 'yellow' }
    login_form = LoginForm()
    register_form = UserForm()
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        if request.FILES:
            login_form = LoginForm()
            register_form = UserForm(request.POST, request.FILES)
            form = register_form
        else:
            login_form = LoginForm(request.POST)
            register_form = UserForm()
            form = login_form
        if form.is_valid():
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                message = request.user.player.message
    return render(request, 'home.html', {'login_form': login_form, 'register_form': register_form, 'team_creation_form': TeamForm(), 'message': message})


def log_out(request):
    logout(request)
    return redirect('/', {'login_form': LoginForm(), 'refister_form': UserForm()})


def registration(request):
    if request.method == 'POST':
        form = UserForm(request.POST, request.FILES)
        if form.is_valid():
            new_usr = User.objects.create_user(username=request.POST.get("username"), password=request.POST.get("password"))
            login(request, new_usr)
            new_player = Player(
                display_name=request.POST.get("display_name"),
                user=new_usr,
                avatar_img=request.FILES['avatar']
            )
            new_player.save()
            return redirect('home')
    return render(request, 'home.html', {'login_form': LoginForm(), 'register_form': UserForm()})


@login_required
def profile_update(request):
    user = request.user.player
    if request.method == 'POST':
        if request.FILES.get('avatar'):
            print(request.FILES.get('avatar'))
        form = UserForm(request.POST, request.FILES)
        player = request.user.player
        player.display_name = request.POST.get('display_name')
        if request.FILES.get('avatar'):
            player.avatar_img = request.FILES.get('avatar')
        player.save()
        return redirect('home')
    else:
        form = UserForm( initial={'display_name':user.display_name, 'team':user.team, 'username': user, 'avatar': user.avatar_img})
    return render(request, 'user/profile_update.html', {'form': form})


@login_required
def new_team(request):
    if request.method == 'POST':
        form = TeamForm(request.POST, request.FILES, {'admin': request.user})
        if form.is_valid:
            player = request.user.player
            new_team = Team(
                name=request.POST.get('team_name'),
                color=request.POST.get('team_color'),
                avatar_img=request.FILES['avatar'],
                admin=player,
                member_count=0,
            )
            new_team.save()
            player.team = new_team
            player.status = 2
            new_team.save()
            player.save()
            return redirect('team', pk=new_team.id)
    else:
        form = TeamForm({'admin': request.user})
    return render(request, 'teams/create_team.html', {'form': form})


@login_required
def team(request, pk):
    team = Team.objects.get(pk=pk)
    req_playa = team.player_set.all()
    current_player = request.user.player
    player_list = []
    for player in team.player_set.all():
        if player != team.admin and player.status == 2:
            player_list.append(player)
    choice = int(request.GET.get('choice'))if request.GET.get('choice') is not None else 10
    paginator = Paginator(player_list, choice)
    page = request.GET.get('page', 1)
    try:
        players = paginator.page(page)
    except PageNotAnInteger:
        players = paginator.page(1)
    except EmptyPage:
        players = paginator.page(paginator.num_pages)
    return render(request, 'teams/team.html', {
        'team': team,
        'message': current_player.message,
        'players': players,
        'player_list': player_list,
        'choice': choice,
        'req_playa': req_playa})


@login_required
def all_teams(request):
    teams = Team.objects.all()
    return render(request, 'teams/all_teams.html', {'teams': teams})


@login_required
def team_requests(request, user_id, req_id, team_id):
    #req_id: 0:join, 1:confirm, 2:deny
    team = Team.objects.get(pk=team_id)
    player = Player.objects.get(pk=user_id)
    if req_id == 0:
        player.status = 1
        player.team = team
    elif req_id == 1:
        player.status = 2
        player.team = team
    elif req_id == 2:
        player.status = 3
        player.team = None
    player.save()
    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def add_usr(request):
    player = request.user.player
    player.team = Team.objects.get(name=request.POST.get('all_team'))
    player.save()
    player.team.save()
    return redirect('team', pk=player.team.id)


@login_required
def leave_team(request, pk):
    requested_user = request.user.player
    if requested_user == requested_user.team.admin:
        admins_team = Team.objects.get(pk=pk)
        all_players = Player.objects.all()
        for player in all_players:
            if player.team == requested_user.team:
                player.status = 3
                player.team = None
                player.save()
        requested_user.team = None
        requested_user.status = 0
        admins_team.delete()
        requested_user.save()
    else:
        requested_user.team = None
        requested_user.status = 0
        requested_user.save()
    return redirect('usr_front_pg')


@login_required
def delete_team_member(request, pk):
    current_player = request.user.player
    team_admin = current_player.team.admin
    if current_player == team_admin:
        player_to_delete = Player.objects.get(pk=pk)
        player_to_delete.team = None
        player_to_delete.save()
        current_player.team.collect_self_members()
    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def chatroom(request, pk):
    team_chat = Team.objects.get(pk=pk)
    return render(request, 'chat/chatroom.html', {'team_chat': team_chat})


@login_required
def get_20_msg(request, pk):
    msg_display = settings.CHAT_MESSAGE_NUMBER_DEFAULT
    requested_user = request.user.player
    team_chat = Team.objects.get(pk=pk)
    message_lst = Messages.objects.filter(team=team_chat).order_by('date').distinct()
    mult = int(request.GET.get('mult'))
    msg_to_pag = []
    if len(message_lst) == 0:
        return render(request, 'chat/chatroom.html',
        {'team_chat': team_chat, 'message_lst': message_lst})
    for message in message_lst:
        msg_to_pag.append(message)
    paginator = Paginator(msg_to_pag[-msg_display*mult:], msg_display)
    if (mult * msg_display) > len(msg_to_pag):
        if len(msg_to_pag) % msg_display == 0:
            sys.exit()
        else:
            if mult*msg_display > len(msg_to_pag)+msg_display:
                sys.exit()
            else:
                paginator = Paginator(msg_to_pag[-msg_display * mult:], len(msg_to_pag)-((mult-1)*msg_display))
    page = request.GET.get('page', 1)
    messages = paginator.page(page)
    data = render_to_string('chat/get_20_msg.html',
    {'messages': messages, 'requested_user': requested_user})
    return JsonResponse(data, safe=False)
