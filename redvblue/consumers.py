import datetime
import json
from channels.generic.websocket import WebsocketConsumer
from asgiref.sync import async_to_sync
from .models import Messages, Player


class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.team_pk = self.scope['url_route']['kwargs']['pk']
        self.team_name = 'game_%s' % self.team_pk
        async_to_sync(self.channel_layer.group_add)(
            self.team_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.team_name,
            self.channel_name
        )

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        player_id = text_data_json['player_id']
        player = Player.objects.get(pk=player_id)
        message_db = Messages(
            team=player.team,
            author=player,
            context=message,
            date=datetime.datetime.now()
        )
        message_db.save()
        async_to_sync(self.channel_layer.group_send)(
            self.team_name,
            {
                'type': 'chat_message',
                'message': player.user.username + ': ' + message,
                'avatar': player.avatar_img.url,
                'team_avi': player.team.avatar_img.url,
            }
        )

    def chat_message(self, event):
        message = event['message']
        avatar = event['avatar']
        team_avi = event['team_avi']
        self.send(text_data=json.dumps({
            'message': message,
            'avatar': str(avatar),
            'team_avi': str(team_avi),
        }))
