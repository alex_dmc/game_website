from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('login/', views.login, name='login'),

    path('log_out/', views.log_out, name='log_out'),
    path('add_usr/', views.add_usr, name='add_usr'),
    path('user/profile_update/', views.profile_update, name='profile_update'),
    path('registration/', views.registration, name='registration'),

    path('new_team/', views.new_team, name='new_team'),
    path('teams/team/<int:pk>/', views.team, name='team'),
    path('teams/all_teams/', views.all_teams, name='all_teams'),
    path('delete_team_member/<int:pk>/', views.delete_team_member, name='delete_team_member'),
    path('leave_team/<int:pk>/', views.leave_team, name='leave_team'),
    path('team_requests/<int:user_id>/<int:req_id>/<int:team_id>/', views.team_requests, name='team_requests'),

    path('chat/chatroom/<int:pk>/', views.chatroom, name='chatroom'),
    path('chat/get_20_msg/<int:pk>/', views.get_20_msg, name='get_20_msg'),

    # path('/', views., name=''),
]
